import cv2
import numpy as np
import torch
from torch import nn, optim
from time import time


def first_layer_sine_init(m):
    """
    Init function for first layer
    """
    with torch.no_grad():
        if hasattr(m, 'weight'):
            num_input = m.weight.size(-1)
            # See paper sec. 3.2, final paragraph, and supplement Sec. 1.5 for discussion of factor 30
            m.weight.uniform_(-1 / num_input, 1 / num_input)


def sine_init(m):
    """
    Init function for layers past the first layer
    """
    with torch.no_grad():
        if hasattr(m, 'weight'):
            num_input = m.weight.size(-1)
            # See supplement Sec. 1.5 for discussion of factor 30
            m.weight.uniform_(-np.sqrt(6 / num_input) / 30, np.sqrt(6 / num_input) / 30)


class SIREN(nn.Module):
    """
    SIREN model class
    """
    def __init__(self, d_input, d_ouput, d_hidden, n_layers):
        super().__init__()
        self.d_hidden = d_hidden
        self.n_layers = n_layers
        self.blocks = [None] * n_layers
        for i in range(n_layers):
            if i == 0:
                self.blocks[i] = nn.Linear(d_input, d_hidden, bias=True)
                first_layer_sine_init(self.blocks[i])
            elif i == n_layers - 1:
                self.blocks[i] = nn.Linear(d_hidden, d_ouput, bias=True)
                sine_init(self.blocks[i])
            else:
                self.blocks[i] = nn.Linear(d_hidden, d_hidden, bias=True)
                sine_init(self.blocks[i])
        self.layers = nn.Sequential(*self.blocks)

    def forward(self, x):
        for i in range(0, self.n_layers - 1):
            x = self.layers[i](x)
            x = torch.sin(30.0 * x)
        x = self.layers[-1](x)
        return x


class SIREN_trainer():
    """
    Trainer class for SIREN
    """
    def __init__(self, image):
        self.input_shape = image.shape
        self.epochs = 1400
        self.batch_size = 2**16
        self.image = image
        self.model = None
        self.use_gpu = None
        self.coords = []
        for i in range(self.image.shape[1]):
            for j in range(self.image.shape[0]):
                self.coords.append([j, i])

    def build_model(self, d_input, d_ouput, d_hidden, n_layers):
        """
        Instanciation function for SIREN model
        """
        self.model = SIREN(d_input, d_ouput, d_hidden, n_layers)
        self.use_gpu = torch.cuda.is_available()
        if self.use_gpu:
            self.model = self.model.cuda()
            self.image = self.image.cuda()
        print(self.model)

    def fit(self, d_input=2, d_ouput=3, d_hidden=512, n_layers=3):
        """
        Train model
        """
        print('d_input', d_input, 'd_ouput', d_ouput, 'd_hidden', d_hidden, 'n_layers', n_layers)
        # get model
        self.build_model(d_input, d_ouput, d_hidden, n_layers)
        # get and print number of parameters
        pytorch_total_params = sum(p.numel() for p in self.model.parameters() if p.requires_grad)
        print('nb learnable parameters :', pytorch_total_params)
        # build loss and optimizer
        criterion = nn.MSELoss()
        optimizer = optim.Adam(self.model.parameters(), lr=0.0001, betas=(0.98, 0.999))
        # get and print number of pixels and distinct values
        train_len = self.image.size(0) * self.image.size(1)
        print('pixels', train_len)
        print('distinct values', train_len*3)
        # start training
        nb_batch = int(np.ceil(train_len / self.batch_size))
        t0 = time()
        for epoch in range(self.epochs):
            running_loss = 0.0
            for i in range(nb_batch):
                # get batch limits
                start = i * self.batch_size
                end = min(train_len, start + self.batch_size)
                batch_size = end - start
                # get batch
                input_ = self.coords[start: end]
                input_ = np.hstack(input_).reshape(batch_size, d_input)
                input_ = torch.Tensor(input_)
                if self.use_gpu:
                    input_ = input_.cuda()
                # infer
                out = self.model(input_)
                # get loss and back propagate
                gold = self.image.view(-1, self.image.size(2))[start:end, :]
                loss = criterion(out, gold)
                running_loss += loss.data.item()
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                print("   %dm: epoch %d  loss = %.6f" %\
                      ((time() - t0)//60, epoch + 1, running_loss/(i+1)),
                      end='\r')
            print()
        return running_loss


if __name__ == '__main__':
    #load picture
    img = cv2.imread('Fractal-tree.jpg', 1)
    print('img.shape', img.shape)
    print(img.reshape(-1, 3).shape)
    print(np.min(img), np.max(img))
    img = img / 255

    # display picture
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # normalize values to [-1; 1]
    img = 2.0 * img - 1.0
    print(np.min(img), np.max(img))

    # build coordonates matrix
    coords = []
    for i in range(img.shape[1]):
        for j in range(img.shape[0]):
            coords.append([j, i])
    coords = np.hstack(coords).reshape(-1, 2)
    coords = torch.Tensor(coords)
    coords = coords.cuda()

    # build pytorch tensor
    img_tensor = torch.Tensor(img).float()

    # instanciate and train model with default settings
    AI = SIREN_trainer(img_tensor)
    AI.fit()

    # infer picture from trained model
    out_img = AI.model(coords)
    out_img = (out_img + 1.0) * 0.5
    out_img = out_img.cpu().detach().numpy().reshape(img.shape[0], img.shape[1], img.shape[2])
    print('out_img', out_img.shape, np.min(out_img), np.max(out_img))

    # display infered picture
    cv2.imshow('out_img', out_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
